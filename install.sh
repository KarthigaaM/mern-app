#!/usr/bin/env bash
# Install ChromeDriver.
#wget -N http://chromedriver.storage.googleapis.com/2.35/chromedriver_linux64.zip -P ~/
sudo wget https://chromedriver.storage.googleapis.com/2.35/chromedriver_linux64.zip
sudo unzip chromedriver_linux64.zip 
sudo rm chromedriver_linux64.zip
sudo mv -f chromedriver /usr/local/bin/chromedriver
# Install Node
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get update
sudo curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add
sudo echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
sudo apt-get -y update
#sudo apt-get -y install google-chrome-stable



